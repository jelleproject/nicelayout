# nicelayout

This collection, called nicelayout, is an implementation of a nice layout in ConTeXt.

Requirements

* It is assumed that you are running the ConTeXt macro package, which is best installed from: https://wiki.contextgarden.net/Installation

* The stylesheet uses the Noto font family from: https://www.google.com/get/noto/ but feel free to change it.

License

This collection is published under a Creative Commons Attribution Share-Alike license.
